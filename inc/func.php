<?php

/*
	Fonction qui renvoie le chemin d'une cover de film
	ou de la cover par defaut
*/
function getPicture($id = null) {

	// On défini le chemin de la cover par defaut
	$cover = 'img/product/product1.jpg';

	// Si la variable $id est définie et supérieure à 0
	if (!empty($id)) {
		// On défini le chemin de la cover d'un film à partir de son id
		$product_cover = 'img/product/'.$id.'';
		// Si le fichier existe sur le serveur
		if (file_exists($product_cover)) {
			// On retourne le chemin de la cover du film
			return $product_cover;
		}
	}
	// On retourne le chemin de la cover par defaut
	return $cover;
}